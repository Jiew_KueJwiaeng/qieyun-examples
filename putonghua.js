/*
 * 施工中。如發現推導結果存在問題亦或是有其它改善建議，歡迎叨擾 @姚嬀潁 jiewkuejwiaeng@outlook.com https://www.zhihu.com/people/yao-gui-ying-39
 */

/* 推導普通話
 *
 * @author graphemecluster
 *
 * 選項『清聲母入聲字声调分派層次』參見平山久雄《中古漢語的清入聲在北京話裏的對應規律》
 * http://www.ncpssd.org/Literature/articleinfo.aspx?id=MTAwMjg3NTkxMQ==&typename=5Lit5paH5pyf5YiK5paH56ug
 *
 * 選項『常母接平聲陰聲韻和船母接平聲韻時讀如……』參見 unt 《爲何中古的dʑ ʑ和普通話讀音的對應似乎是反的（即dʑ > ʂ、ʑ > ʈʂ）？》
 * https://www.zhihu.com/question/526195183/answer/2425807330
 *細音佳韻
 */

const is = (x) => 音韻地位.屬於(x);

if (!音韻地位) return [
  ['$legacy', true],
  ['常母接平聲陰聲韻和船母接平聲韻時讀如', [1, '生母 [ʂ]', '初母 [tʂʰ]']],
  ['疑母讀如', [1, '影母 [ʔ]', '泥母 [n]', '疑母 [ŋ]']],
  ['匣母讀如', [1, '許母 [x]', '居母 [k]']],
  ['分尖團', false],
  ['遇攝三等接知系聲母時讀如', [1, '尤韻 [u]', '希韻 [y]', '脂韻 [ɻ̍]']],
  ['存在弱合法韻母', false],
  ['一等歌韻讀如', [1, '虞韻 [wo]', '模韻 [o]', '魚韻 [ɤ]', '麻韻 [a]']],
  ['全濁上歸去', true],
  ['入聲韻分化層次', [1, '主流層', '白讀層', '文讀層', '暴力白讀層']],
  ['清聲母入聲調分派層次', [2, 'A 皆派入上聲', 'B 皆派入陰平', 'C 除影母和擦音聲母字派入去聲，其餘派入陽平', 'D 除影母字派入去聲，其餘派入陽平', 'E 連同濁聲母、所有入聲調全部派入去聲']],
  ['書寫系統', [1, '漢語拼音', '國際音標']],
  ['漢語拼音注釋改寫', true], 
  ['應用注釋9', false], 
  ['標調方式', [1, '聲調符號', '四角標圈', '五度標記折綫式', '五度標記數字式', '方案序數', '不標調']],
];

function 聲母推導規則() {
  if (['漢語拼音', '國際音標'].includes(選項.書寫系統)) {
    const idx = ['漢語拼音', '國際音標'].indexOf(選項.書寫系統);
    if (is('幫組 幫滂並母 輕脣韻')) return ['f', 'f'][idx];
    if (is('幫組 幫母')) return ['b', 'p'][idx];
    if (is('幫組 滂母')) return ['p', 'pʰ'][idx];
    if (is('幫組 並母 平聲')) return ['p', 'pʰ'][idx];
    if (is('幫組 並母 仄聲')) return ['b', 'p'][idx];
    if (is('幫組 明母 微虞文元陽凡韻')) return ['w', ''][idx];
    if (is('幫組 明母')) return ['m', 'm'][idx];
    if (is('端組 端母')) return ['d', 't'][idx];
    if (is('端組 透母')) return ['t', 'tʰ'][idx];
    if (is('端組 定母 平聲')) return ['t', 'tʰ'][idx];
    if (is('端組 定母 仄聲')) return ['d', 't'][idx];
    if (is('端組 泥母')) return ['n', 'n'][idx];
    if (is('來母')) return ['l', 'l'][idx];
    if (is('知組 知母')) return ['zh', 'tʂ'][idx];
    if (is('知組 徹母')) return ['ch', 'tʂʰ'][idx];
    if (is('知組 澄母 平聲')) return ['ch', 'tʂʰ'][idx];
    if (is('知組 澄母 仄聲')) return ['zh', 'tʂ'][idx];
    if (is('知組 孃母')) return ['n', 'n'][idx];
    if (is('精組 精母')) return ['z', 'ts'][idx];
    if (is('精組 清母')) return ['c', 'tsʰ'][idx];
    if (is('精組 從母 平聲')) return ['c', 'tsʰ'][idx];
    if (is('精組 從母 仄聲')) return ['z', 'ts'][idx];
    if (is('精組 心母')) return ['s', 's'][idx];
    if (is('精組 邪母')) return ['s', 's'][idx];
    if (is('莊組 莊母')) return ['zh', 'tʂ'][idx];
    if (is('莊組 初母')) return ['ch', 'tʂʰ'][idx];
    if (is('莊組 崇母 平聲')) return ['ch', 'tʂʰ'][idx];
    if (is('莊組 崇母 仄聲')) return ['zh', 'tʂ'][idx];
    if (is('莊組 生母')) return ['sh', 'ʂ'][idx];
    if (is('莊組 俟母')) return ['sh', 'ʂ'][idx];
    if (is('章組 章母')) return ['zh', 'tʂ'][idx];
    if (is('章組 昌母')) return ['ch', 'tʂʰ'][idx];
    if (is('章組 常母 平聲 陽聲韻')) return ['ch', 'tʂʰ'][idx];
    if (選項.常母接平聲陰聲韻和船母接平聲韻時讀如 === '生母 [ʂ]') {
      if (['漢語拼音', '國際音標'].includes(選項.書寫系統)) {
        const idx = ['漢語拼音', '國際音標'].indexOf(選項.書寫系統);
        if (is('章組 常母 平聲 陰聲韻')) return ['sh', 'ʂ'][idx];
      }
    }
    if (選項.常母接平聲陰聲韻和船母接平聲韻時讀如 === '初母 [tʂʰ]') {
      if (['漢語拼音', '國際音標'].includes(選項.書寫系統)) {
        const idx = ['漢語拼音', '國際音標'].indexOf(選項.書寫系統);
        if (is('章組 常母 平聲 陰聲韻')) return ['ch', 'tʂʰ'][idx];
      }
    }
    if (is('章組 常母 仄聲')) return ['sh', 'ʂ'][idx];
    if (is('章組 書母')) return ['sh', 'ʂ'][idx];
    if (選項.常母接平聲陰聲韻和船母接平聲韻時讀如 === '生母 [ʂ]') {
      if (['漢語拼音', '國際音標'].includes(選項.書寫系統)) {
        const idx = ['漢語拼音', '國際音標'].indexOf(選項.書寫系統);
        if (is('章組 船母 平聲')) return ['sh', 'ʂ'][idx];
      }
    }
    if (選項.常母接平聲陰聲韻和船母接平聲韻時讀如 === '初母 [tʂʰ]') {
      if (['漢語拼音', '國際音標'].includes(選項.書寫系統)) {
        const idx = ['漢語拼音', '國際音標'].indexOf(選項.書寫系統);
        if (is('章組 船母 平聲')) return ['ch', 'tʂʰ'][idx];
      }
    }
    if (is('章組 船母 仄聲')) return ['sh', 'ʂ'][idx];
    if (is('日母')) return ['r', 'ɻ'][idx];
    if (is('見組 見母')) return ['g', 'k'][idx];
    if (is('見組 溪母')) return ['k', 'kʰ'][idx];
    if (is('見組 羣母 平聲')) return ['k', 'kʰ'][idx];
    if (is('見組 羣母 仄聲')) return ['g', 'k'][idx];
    if (選項.疑母讀如 === '影母 [ʔ]') {
      if (['漢語拼音', '國際音標'].includes(選項.書寫系統)) {
        const idx = ['漢語拼音', '國際音標'].indexOf(選項.書寫系統);
        if (is('見組 疑母')) return ['', ''][idx];
      }
    }
    if (選項.疑母讀如 === '泥母 [n]') {
      if (['漢語拼音', '國際音標'].includes(選項.書寫系統)) {
        const idx = ['漢語拼音', '國際音標'].indexOf(選項.書寫系統);
        if (is('見組 疑母')) return ['n', 'n'][idx];
      }
    }
    if (選項.疑母讀如 === '疑母 [ŋ]') {
      if (['漢語拼音', '國際音標'].includes(選項.書寫系統)) {
        const idx = ['漢語拼音', '國際音標'].indexOf(選項.書寫系統);
        if (is('見組 疑母')) return ['ng', 'ŋ'][idx];
      }
    }
    if (is('影組 影母')) return ['', ''][idx];
    if (is('影組 曉母')) return ['h', 'x'][idx];
    if (選項.匣母讀如 === '許母 [x]') {
      if (['漢語拼音', '國際音標'].includes(選項.書寫系統)) {
        const idx = ['漢語拼音', '國際音標'].indexOf(選項.書寫系統);
        if (is('影組 匣母')) return ['h', 'x'][idx];
      }
    }
    if (選項.匣母讀如 === '居母 [k]') {
      if (['漢語拼音', '國際音標'].includes(選項.書寫系統)) {
        const idx = ['漢語拼音', '國際音標'].indexOf(選項.書寫系統);
        if (is('影組 匣母')) return ['g', 'k'][idx];
      }
    }
    if (is('影組 云母')) return ['', ''][idx];
    if (is('以母')) return ['', ''][idx];
  }
}

function 舒聲韻母規則() {
  if (['漢語拼音', '國際音標'].includes(選項.書寫系統)) {
    const idx = ['漢語拼音', '國際音標'].indexOf(選項.書寫系統);
    if (is('通攝 三等 牙喉音')) return ['iong', 'jʊŋ'][idx];
    if (!選項.存在弱合法韻母) {
      if (['漢語拼音', '國際音標'].includes(選項.書寫系統)) {
        const idx = ['漢語拼音', '國際音標'].indexOf(選項.書寫系統);
        if (is('通攝 幫組')) return ['eng', 'ɤŋ'][idx];
      }
    }
    if (is('通攝')) return ['ong', 'ʊŋ'][idx];
    if (is('江攝 江韻 牙喉音')) return ['iang', 'jɑŋ'][idx];
    if (is('江攝 江韻 幫組')) return ['ang', 'ɑŋ'][idx];
    if (is('江攝 江韻')) return ['uang', 'wɑŋ'][idx];
    if (選項.存在弱合法韻母) {
      if (['漢語拼音', '國際音標'].includes(選項.書寫系統)) {
        const idx = ['漢語拼音', '國際音標'].indexOf(選項.書寫系統);
        if (is('止攝 微韻 幫組')) return ['i', 'i'][idx];
      }
    }
    if (is('止攝 微韻 幫組')) return ['ei', 'eɪ'][idx];
    if (!選項.存在弱合法韻母) {
      if (['漢語拼音', '國際音標'].includes(選項.書寫系統)) {
        const idx = ['漢語拼音', '國際音標'].indexOf(選項.書寫系統);
        if (is('止攝 合口 來母')) return ['ei', 'eɪ'][idx];
      }
    }
    if (is('止攝 合口 莊組')) return ['uai', 'waɪ'][idx];
    if (is('止攝 合口')) return ['uei', 'weɪ'][idx];
    if (is('止攝 牙喉音')) return ['i', 'i'][idx];
    if (選項.存在弱合法韻母) {
      if (['漢語拼音', '國際音標'].includes(選項.書寫系統)) {
        const idx = ['漢語拼音', '國際音標'].indexOf(選項.書寫系統);
        if (is('止攝 知組') && !is('孃母')) return ['i', 'i'][idx];
        if (is('止攝 莊組')) return ['i', 'i'][idx];
        if (is('止攝 章組')) return ['i', 'i'][idx];
        if (is('止攝 日母')) return ['er', 'əɻ'][idx];
        if (is('止攝 精組')) return ['i', 'i'][idx];
      }
    }
    if (is('止攝 知組') && !is('孃母')) return ['-i', 'ɻ̍'][idx];
    if (is('止攝 莊組')) return ['-i', 'ɻ̍'][idx];
    if (is('止攝 章組')) return ['-i', 'ɻ̍'][idx];
    if (is('止攝 日母')) return ['er', 'əɻ'][idx];
    if (is('止攝 精組')) return ['-i', 'ɹ̩'][idx];
    if (is('止攝')) return ['i', 'i'][idx];
    // if (is('止攝')) return ['ei', 'eɪ'][idx];
    if (選項.遇攝三等接知系聲母時讀如 === '尤韻 [u]') {
      if (['漢語拼音', '國際音標'].includes(選項.書寫系統)) {
        const idx = ['漢語拼音', '國際音標'].indexOf(選項.書寫系統);
        if ((is('遇攝 魚韻 知組')) && (!is('孃母'))) return ['u', 'u'][idx];
        if (is('遇攝 魚韻 莊組')) return ['u', 'u'][idx];
        if (is('遇攝 魚韻 章組')) return ['u', 'u'][idx];
      }
    }
    if (選項.遇攝三等接知系聲母時讀如 === '希韻 [y]') {
      if (['漢語拼音', '國際音標'].includes(選項.書寫系統)) {
        const idx = ['漢語拼音', '國際音標'].indexOf(選項.書寫系統);
        if ((is('遇攝 魚韻 知組')) && (!is('孃母'))) return ['ü', 'ʯ'][idx];
        if (is('遇攝 魚韻 莊組')) return ['u', 'u'][idx];
        if (is('遇攝 魚韻 章組')) return ['ü', 'ʯ'][idx];
      }
    }
    if (選項.遇攝三等接知系聲母時讀如 === '脂韻 [ɻ̍]') {
      if (['漢語拼音', '國際音標'].includes(選項.書寫系統)) {
        const idx = ['漢語拼音', '國際音標'].indexOf(選項.書寫系統);
        if ((is('遇攝 魚韻 知組')) && (!is('孃母'))) return ['-i', 'ɻ̍'][idx];
        if (is('遇攝 魚韻 莊組')) return ['u', 'u'][idx];
        if (is('遇攝 魚韻 章組')) return ['-i', 'ɻ̍'][idx];
      }
    }
    if (is('遇攝 魚韻 日母')) return ['u', 'u'][idx];
    if (is('遇攝 魚韻')) return ['ü', 'y'][idx];
    if (is('遇攝 虞韻 幫組')) return ['u', 'u'][idx];
    if (選項.遇攝三等接知系聲母時讀如 === '尤韻 [u]') {
      if (['漢語拼音', '國際音標'].includes(選項.書寫系統)) {
        const idx = ['漢語拼音', '國際音標'].indexOf(選項.書寫系統);
        if ((is('遇攝 虞韻 知組')) && (!is('孃母'))) return ['u', 'u'][idx];
        if (is('遇攝 虞韻 莊組')) return ['u', 'u'][idx];
        if (is('遇攝 虞韻 章組')) return ['u', 'u'][idx];
      }
    }
    if (選項.遇攝三等接知系聲母時讀如 === '希韻 [y]') {
      if (['漢語拼音', '國際音標'].includes(選項.書寫系統)) {
        const idx = ['漢語拼音', '國際音標'].indexOf(選項.書寫系統);
        if ((is('遇攝 魚韻 知組')) && (!is('孃母'))) return ['ü', 'ʯ'][idx];
        if (is('遇攝 魚韻 莊組')) return ['u', 'u'][idx];
        if (is('遇攝 魚韻 章組')) return ['ü', 'ʯ'][idx];
      }
    }
    if (選項.遇攝三等接知系聲母時讀如 === '脂韻 [ɻ̍]') {
      if (['漢語拼音', '國際音標'].includes(選項.書寫系統)) {
        const idx = ['漢語拼音', '國際音標'].indexOf(選項.書寫系統);
        if (is('遇攝 虞韻 知組') && !is('孃母')) return ['-i', 'ɻ̍'][idx];
        if (is('遇攝 虞韻 莊組')) return ['u', 'u'][idx];
        if (is('遇攝 虞韻 章組')) return ['-i', 'ɻ̍'][idx];
      }
    }
    if (is('遇攝 虞韻 日母')) return ['u', 'u'][idx];
    if (is('遇攝 虞韻')) return ['ü', 'y'][idx];
    if (is('遇攝 模韻')) return ['u', 'u'][idx];
    if (is('蟹攝 齊韻 合口')) return ['uei', 'weɪ'][idx];
    if (is('蟹攝 齊韻')) return ['i', 'i'][idx];
    if (is('蟹攝 祭韻 合口 莊組')) return ['uai', 'waɪ'][idx];
    if (is('蟹攝 祭韻 合口')) return ['uei', 'weɪ'][idx];
    if (is('蟹攝 祭韻')) return ['i', 'i'][idx];
    if (is('蟹攝 泰韻 開口')) return ['ai', 'aɪ'][idx];
    if (is('蟹攝 泰韻')) return ['uei', 'weɪ'][idx];
    if (is('蟹攝 佳韻 牙喉音 合口')) return ['ua', 'wa'][idx];
    if (選項.存在弱合法韻母) {
      if (['漢語拼音', '國際音標'].includes(選項.書寫系統)) {
        const idx = ['漢語拼音', '國際音標'].indexOf(選項.書寫系統);
        if (is('蟹攝 佳韻 牙喉音')) return ['iai', 'jæɪ'][idx];
      }
    }
    if (is('蟹攝 佳韻 牙喉音')) return ['ia', 'ja'][idx];
    if (is('蟹攝 佳韻 合口')) return ['uai', 'waɪ'][idx];
    if (is('蟹攝 佳韻')) return ['ai', 'aɪ'][idx];
    if (is('蟹攝 皆韻 牙喉音 合口')) return ['uai', 'waɪ'][idx];
    if (選項.存在弱合法韻母) {
      if (['漢語拼音', '國際音標'].includes(選項.書寫系統)) {
        const idx = ['漢語拼音', '國際音標'].indexOf(選項.書寫系統);
        if (is('蟹攝 皆韻 牙喉音')) return ['iai', 'jæɪ'][idx];
      }
    }
    if (is('蟹攝 皆韻 牙喉音')) return ['ie', 'jɛ'][idx];
    if (is('蟹攝 皆韻 合口')) return ['uai', 'waɪ'][idx];
    if (is('蟹攝 皆韻')) return ['ai', 'aɪ'][idx];
    if (is('蟹攝 夬韻 牙喉音 合口')) return ['uai', 'waɪ'][idx];
    if (is('蟹攝 夬韻 牙喉音')) return ['ie', 'jɛ'][idx];
    if (is('蟹攝 夬韻 合口')) return ['uai', 'waɪ'][idx];
    if (is('蟹攝 夬韻')) return ['ai', 'aɪ'][idx];
    if (is('蟹攝 灰韻 開口')) return ['i', 'i'][idx];
    if (is('蟹攝 灰韻 幫組')) return ['ei', 'eɪ'][idx];
    if (is('蟹攝 灰韻')) return ['uei', 'weɪ'][idx];
    if (is('蟹攝 咍韻')) return ['ai', 'aɪ'][idx];
    if (is('蟹攝 廢韻 合口')) return ['uei', 'weɪ'][idx];
    if (!選項.存在弱合法韻母) {
      if (['漢語拼音', '國際音標'].includes(選項.書寫系統)) {
        const idx = ['漢語拼音', '國際音標'].indexOf(選項.書寫系統);
        if (is('蟹攝 廢韻 幫組')) return ['ei', 'eɪ'][idx];
      }
    }
    if (is('蟹攝 廢韻')) return ['i', 'i'][idx];
    if (is('臻攝 眞韻 合口 來母')) return ['uen', 'wən'][idx];
    if ((is('臻攝 眞韻 合口 知組')) && (!is('孃母'))) return ['uen', 'wən'][idx];
    if (is('臻攝 眞韻 合口 莊組')) return ['uen', 'wən'][idx];
    if (is('臻攝 眞韻 合口 章組')) return ['uen', 'wən'][idx];
    if (is('臻攝 眞韻 合口 日母')) return ['uen', 'wən'][idx];
    if (is('臻攝 眞韻 合口')) return ['ün', 'yn'][idx];
    if ((is('臻攝 眞韻 知組')) && (!is('孃母'))) return ['en', 'ən'][idx];
    if (is('臻攝 眞韻 莊組')) return ['en', 'ən'][idx];
    if (is('臻攝 眞韻 章組')) return ['en', 'ən'][idx];
    if (is('臻攝 眞韻 日母')) return ['en', 'ən'][idx];
    if (is('臻攝 眞韻')) return ['in', 'in'][idx];
    if (is('臻攝 臻韻')) return ['en', 'ən'][idx];
    if (is('臻攝 文韻 牙喉音')) return ['ün', 'yn'][idx];
    if (is('臻攝 文韻 幫組')) return ['en', 'wən'][idx];
    if (is('臻攝 文韻')) return ['uen', 'wən'][idx];
    if (is('臻攝 欣韻')) return ['in', 'in'][idx];
    if (is('臻攝 元韻 合口')) return ['üan', 'ɥæn'][idx];
    if (is('臻攝 元韻 幫組')) return ['an', 'wan'][idx];
    if (is('臻攝 元韻')) return ['ian', 'jɛn'][idx];
    if (is('臻攝 魂韻 幫組')) return ['en', 'wən'][idx];
    if (is('臻攝 魂韻')) return ['uen', 'wən'][idx];
    if (is('臻攝 痕韻 牙喉音')) return ['en', 'ən'][idx];
    if (is('臻攝 痕韻')) return ['uen', 'wən'][idx];
    if (is('山攝 寒韻 合口')) return ['uan', 'wan'][idx];
    if (選項.存在弱合法韻母) {
      if (['漢語拼音', '國際音標'].includes(選項.書寫系統)) {
        const idx = ['漢語拼音', '國際音標'].indexOf(選項.書寫系統);
        if (is('山攝 寒韻 幫組')) return ['uan', 'wan'][idx];
      }
    }
    if (is('山攝 寒韻')) return ['an', 'an'][idx];
    if (is('山攝 刪韻 合口')) return ['uan', 'wan'][idx];
    if (is('山攝 刪韻 牙喉音')) return ['ian', 'jɛn'][idx];
    if (選項.存在弱合法韻母) {
      if (['漢語拼音', '國際音標'].includes(選項.書寫系統)) {
        const idx = ['漢語拼音', '國際音標'].indexOf(選項.書寫系統);
        if (is('山攝 刪韻 幫組')) return ['uan', 'wan'][idx];
      }
    }
    if (is('山攝 刪韻')) return ['an', 'an'][idx];
    if (is('山攝 山韻 合口')) return ['uan', 'wan'][idx];
    if (is('山攝 山韻 牙喉音')) return ['ian', 'jɛn'][idx];
    if (is('山攝 山韻')) return ['an', 'an'][idx];
    if (is('山攝 先韻 合口')) return ['üan', 'ɥæn'][idx];
    if (is('山攝 先韻')) return ['ian', 'jɛn'][idx];
    if ((is('山攝 仙韻 合口 知組')) && (!is('孃母'))) return ['uan', 'wan'][idx];
    if (is('山攝 仙韻 合口 莊組')) return ['uan', 'wan'][idx];
    if (is('山攝 仙韻 合口 章組')) return ['uan', 'wan'][idx];
    if (is('山攝 仙韻 合口 日母')) return ['uan', 'wan'][idx];
    if (is('山攝 仙韻 合口')) return ['üan', 'ɥæn'][idx];
    if ((is('山攝 仙韻 知組')) && (!is('孃母'))) return ['an', 'an'][idx];
    if (is('山攝 仙韻 莊組')) return ['an', 'an'][idx];
    if (is('山攝 仙韻 章組')) return ['an', 'an'][idx];
    if (is('山攝 仙韻 日母')) return ['an', 'an'][idx];
    if (is('山攝 仙韻')) return ['ian', 'jɛn'][idx];
    if (is('效攝 蕭韻')) return ['iao', 'jɑʊ'][idx];
    if ((is('效攝 宵韻 知組')) && (!is('孃母'))) return ['ao', 'ɑʊ'][idx];
    if (is('效攝 宵韻 莊組')) return ['ao', 'ɑʊ'][idx];
    if (is('效攝 宵韻 章組')) return ['ao', 'ɑʊ'][idx];
    if (is('效攝 宵韻 日母')) return ['ao', 'ɑʊ'][idx];
    if (is('效攝 宵韻')) return ['iao', 'jɑʊ'][idx];
    if (is('效攝 肴韻 牙喉音')) return ['iao', 'jɑʊ'][idx];
    if (is('效攝 肴韻')) return ['ao', 'ɑʊ'][idx];
    if (is('效攝 豪韻')) return ['ao', 'ɑʊ'][idx];
    // if (is('果攝 歌韻 一等 開口 牙喉音')) return ['e', 'ɤ'][idx];
    if (選項.一等歌韻讀如 === '虞韻 [wo]') {
      if (['漢語拼音', '國際音標'].includes(選項.書寫系統)) {
        const idx = ['漢語拼音', '國際音標'].indexOf(選項.書寫系統);
        if (is('果攝 歌韻 一等 幫組')) return ['o', 'u̯o'][idx];
        if (is('果攝 歌韻 一等')) return ['uo', 'wo'][idx];
      }
    }
    if (選項.一等歌韻讀如 === '模韻 [o]') {
      if (['漢語拼音', '國際音標'].includes(選項.書寫系統)) {
        const idx = ['漢語拼音', '國際音標'].indexOf(選項.書寫系統);
        if (is('果攝 歌韻 一等 幫組')) return ['o', 'u̯o'][idx];
        if (is('果攝 歌韻 一等')) return ['o', 'o'][idx];
      }
    }
    if (選項.一等歌韻讀如 === '魚韻 [ɤ]') {
      if (['漢語拼音', '國際音標'].includes(選項.書寫系統)) {
        const idx = ['漢語拼音', '國際音標'].indexOf(選項.書寫系統);
        if (is('果攝 歌韻 一等 幫組')) return ['o', 'u̯o'][idx];
        if (is('果攝 歌韻 一等')) return ['e', 'ɤ'][idx];
      }
    }
    if (選項.一等歌韻讀如 === '麻韻 [a]') {
      if (['漢語拼音', '國際音標'].includes(選項.書寫系統)) {
        const idx = ['漢語拼音', '國際音標'].indexOf(選項.書寫系統);
        if (is('果攝 歌韻 一等')) return ['a', 'a'][idx];
      }
    }
    if (is('果攝 歌韻 三等 開口')) return ['ie', 'jɛ'][idx];
    if (is('果攝 歌韻 三等')) return ['üe', 'ɥɛ'][idx];
    if (is('假攝 麻韻 二等 合口')) return ['ua', 'wa'][idx];
    if (is('假攝 麻韻 二等 牙喉音')) return ['ia', 'ja'][idx];
    if (is('假攝 麻韻 二等')) return ['a', 'a'][idx];
    if ((is('假攝 麻韻 三等 知組')) && (!is('孃母'))) return ['e', 'ɤ'][idx];
    if (is('假攝 麻韻 三等 莊組')) return ['e', 'ɤ'][idx];
    if (is('假攝 麻韻 三等 章組')) return ['e', 'ɤ'][idx];
    if (is('假攝 麻韻 三等 日母')) return ['e', 'ɤ'][idx];
    if (is('假攝 麻韻 三等')) return ['ie', 'jɛ'][idx];
    if ((is('宕攝 陽韻 開口 知組')) && (!is('孃母'))) return ['ang', 'ɑŋ'][idx];
    if (is('宕攝 陽韻 開口 莊組')) return ['uang', 'wɑŋ'][idx];
    if (is('宕攝 陽韻 開口 章組')) return ['ang', 'ɑŋ'][idx];
    if (is('宕攝 陽韻 開口 日母')) return ['ang', 'ɑŋ'][idx];
    if (is('宕攝 陽韻 開口')) return ['iang', 'jɑŋ'][idx];
    if (is('宕攝 陽韻 幫組')) return ['ang', 'ɑŋ'][idx];
    if (is('宕攝 陽韻')) return ['uang', 'wɑŋ'][idx];
    if (is('宕攝 唐韻 開口')) return ['ang', 'ɑŋ'][idx];
    if (is('宕攝 唐韻')) return ['uang', 'wɑŋ'][idx];
    if (is('梗攝 二等 合口')) return ['ong', 'ʊŋ'][idx];
    if (選項.存在弱合法韻母) {
      if (['漢語拼音', '國際音標'].includes(選項.書寫系統)) {
        const idx = ['漢語拼音', '國際音標'].indexOf(選項.書寫系統);
        if (is('梗攝 二等 幫組')) return ['ong', 'ʊŋ'][idx];
      }
    }
    if (is('梗攝 二等')) return ['eng', 'ɤŋ'][idx];
    if (is('梗攝 合口')) return ['iong', 'jʊŋ'][idx];
    if (is('梗攝 知組') && !is('孃母')) return ['eng', 'ɤŋ'][idx];
    if (is('梗攝 莊組')) return ['eng', 'ɤŋ'][idx];
    if (is('梗攝 章組')) return ['eng', 'ɤŋ'][idx];
    if (is('梗攝 日母')) return ['eng', 'ɤŋ'][idx];
    if (is('梗攝')) return ['ing', 'iŋ'][idx];
    if (is('曾攝 登韻 合口')) return ['ong', 'ʊŋ'][idx];
    if (選項.存在弱合法韻母) {
      if (['漢語拼音', '國際音標'].includes(選項.書寫系統)) {
        const idx = ['漢語拼音', '國際音標'].indexOf(選項.書寫系統);
        if (is('曾攝 登韻 幫組')) return ['ong', 'ʊŋ'][idx];
      }
    }
    if (is('曾攝 登韻')) return ['eng', 'ɤŋ'][idx];
    if (is('曾攝 蒸韻 知組') && !is('孃母')) return ['eng', 'ɤŋ'][idx];
    if (is('曾攝 蒸韻 莊組')) return ['eng', 'ɤŋ'][idx];
    if (is('曾攝 蒸韻 章組')) return ['eng', 'ɤŋ'][idx];
    if (is('曾攝 蒸韻 日母')) return ['eng', 'ɤŋ'][idx];
    if (is('曾攝 蒸韻')) return ['ing', 'iŋ'][idx];
    if (is('流攝 侯韻')) return ['ou', 'oʊ'][idx];
    if (is('流攝 尤韻 幫組')) return ['ou', 'oʊ'][idx];
    if ((is('流攝 尤韻 知組')) && (!is('孃母'))) return ['ou', 'oʊ'][idx];
    if (is('流攝 尤韻 莊組')) return ['ou', 'oʊ'][idx];
    if (is('流攝 尤韻 章組')) return ['ou', 'oʊ'][idx];
    if (is('流攝 尤韻 日母')) return ['ou', 'oʊ'][idx];
    if (is('流攝 尤韻')) return ['iou', 'joʊ'][idx];
    if (is('流攝 幽韻 幫組')) return ['iao', 'jɑʊ'][idx];
    if (is('流攝 幽韻')) return ['iou', 'joʊ'][idx];
    if ((is('深攝 侵韻 知組')) && (!is('孃母'))) return ['en', 'ən'][idx];
    if (is('深攝 侵韻 莊組')) return ['en', 'ən'][idx];
    if (is('深攝 侵韻 章組')) return ['en', 'ən'][idx];
    if (is('深攝 侵韻 日母')) return ['en', 'ən'][idx];
    if (is('深攝 侵韻')) return ['in', 'in'][idx];
    if (is('咸攝 覃韻')) return ['an', 'an'][idx];
    if (is('咸攝 談韻')) return ['an', 'an'][idx];
    if ((is('咸攝 鹽韻 知組')) && (!is('孃母'))) return ['an', 'an'][idx];
    if (is('咸攝 鹽韻 莊組')) return ['an', 'an'][idx];
    if (is('咸攝 鹽韻 章組')) return ['an', 'an'][idx];
    if (is('咸攝 鹽韻 日母')) return ['an', 'an'][idx];
    if (is('咸攝 鹽韻')) return ['ian', 'jɛn'][idx];
    if (is('咸攝 添韻')) return ['ian', 'jɛn'][idx];
    if (is('咸攝 嚴咸銜韻 牙喉音')) return ['ian', 'jɛn'][idx];
    if (is('咸攝 嚴咸銜韻')) return ['an', 'an'][idx];
    if (is('咸攝 凡韻 幫組')) return ['an', 'an'][idx];
    if (is('咸攝 凡韻')) return ['uan', 'wan'][idx];
  }
}

function 入聲韻母規則() {
  if (選項.入聲韻分化層次 === '主流層') {
    if (['漢語拼音', '國際音標'].includes(選項.書寫系統)) {
      const idx = ['漢語拼音', '國際音標'].indexOf(選項.書寫系統);
      if (is('通攝 一等')) return ['u', 'u'][idx];
      if (is('通攝 三等 幫組')) return ['u', 'u'][idx];
      if (is('通攝 三等 精組')) return ['u', 'u'][idx];
      if (is('通攝 三等 知組')) return ['u', 'u'][idx];
      if (is('通攝 三等 莊組')) return ['u', 'u'][idx];
      if (is('通攝 三等 章組')) return ['u', 'u'][idx];
      if (is('通攝 三等 日母')) return ['u', 'u'][idx];
      if (is('通攝 三等')) return ['ü', 'y'][idx];
      if (選項.存在弱合法韻母) {
        if (['漢語拼音', '國際音標'].includes(選項.書寫系統)) {
          const idx = ['漢語拼音', '國際音標'].indexOf(選項.書寫系統);
          if (is('江攝 江韻 牙喉音')) return ['io', 'jɔ'][idx];
        }
      }
      if (is('江攝 江韻 牙喉音')) return ['üe', 'ɥɛ'][idx];
      if (is('江攝 江韻 幫組')) return ['o', 'u̯o'][idx];
      if (is('江攝 江韻')) return ['uo', 'wo'][idx];
      if (is('臻攝 眞韻 合口 知組') && !is('孃母')) return ['u', 'u'][idx];
      if (is('臻攝 眞韻 合口 莊組')) return ['uai', 'waɪ'][idx];
      if (is('臻攝 眞韻 合口 章組')) return ['u', 'u'][idx];
      if (is('臻攝 眞韻 合口 日母')) return ['u', 'u'][idx];
      if (is('臻攝 眞韻 合口')) return ['ü', 'y'][idx];
      if (is('臻攝 眞欣韻')) return ['i', 'i'][idx];
      if (is('臻攝 臻痕韻')) return ['e', 'ɤ'][idx];
      if (is('臻攝 魂韻 幫組')) return ['o', 'o'][idx];
      if (is('臻攝 魂韻')) return ['u', 'u'][idx];
      if (is('臻攝 文韻 幫組')) return ['u', 'u'][idx];
      if (is('臻攝 文韻')) return ['ü', 'y'][idx];
      if (is('臻攝 元韻 開口')) return ['ie', 'jɛ'][idx];
      if (is('臻攝 元韻 牙喉音')) return ['üe', 'ɥɛ'][idx];
      if (is('臻攝 元韻')) return ['a', 'a'][idx];
      if (is('山攝 寒韻 開口 牙喉音')) return ['e', 'ɤ'][idx];
      if (is('山攝 寒韻 開口')) return ['a', 'a'][idx];
      if (is('山攝 寒韻 一等 幫組')) return ['o', 'u̯o'][idx];
      if (is('山攝 寒韻')) return ['uo', 'wo'][idx];
      if (is('山攝 刪韻 合口')) return ['ua', 'wa'][idx];
      if (is('山攝 刪韻 牙喉音')) return ['ia', 'ja'][idx];
      if (is('山攝 刪韻')) return ['a', 'a'][idx];
      if (is('山攝 山韻 合口')) return ['ua', 'wa'][idx];
      if (is('山攝 山韻 牙喉音')) return ['ia', 'ja'][idx];
      if (is('山攝 山韻')) return ['a', 'a'][idx];
      if (is('山攝 先韻 合口')) return ['üe', 'ɥɛ'][idx];
      if (is('山攝 先韻')) return ['ie', 'jɛ'][idx];
      if (is('山攝 仙韻 合口 知組') && !is('孃母')) return ['uo', 'wo'][idx];
      if (is('山攝 仙韻 合口 莊組')) return ['uo', 'wo'][idx];
      if (is('山攝 仙韻 合口 章組')) return ['uo', 'wo'][idx];
      if (is('山攝 仙韻 合口 日母')) return ['uo', 'wo'][idx];
      if (is('山攝 仙韻 合口')) return ['üe', 'ɥɛ'][idx];
      if (is('山攝 仙韻 知組') && !is('孃母')) return ['e', 'ɤ'][idx];
      if (is('山攝 仙韻 莊組')) return ['e', 'ɤ'][idx];
      if (is('山攝 仙韻 章組')) return ['e', 'ɤ'][idx];
      if (is('山攝 仙韻 日母')) return ['e', 'ɤ'][idx];
      if (is('山攝 仙韻')) return ['ie', 'jɛ'][idx];
      if (選項.存在弱合法韻母) {
        if (['漢語拼音', '國際音標'].includes(選項.書寫系統)) {
          const idx = ['漢語拼音', '國際音標'].indexOf(選項.書寫系統);
          if (is('宕攝 陽韻 知組') && !is('孃母')) return ['io', 'jɔ'][idx];
          if (is('宕攝 陽韻 莊組')) return ['io', 'jɔ'][idx];
          if (is('宕攝 陽韻 章組')) return ['io', 'jɔ'][idx];
          if (is('宕攝 陽韻 日母')) return ['io', 'jɔ'][idx];
        }
      }
      if (is('宕攝 陽韻 幫組')) return ['o', 'o'][idx];
      if (is('宕攝 陽韻 知組') && !is('孃母')) return ['uo', 'wo'][idx];
      if (is('宕攝 陽韻 莊組')) return ['uo', 'wo'][idx];
      if (is('宕攝 陽韻 章組')) return ['uo', 'wo'][idx];
      if (is('宕攝 陽韻 日母')) return ['uo', 'wo'][idx];
      if (選項.存在弱合法韻母) {
        if (['漢語拼音', '國際音標'].includes(選項.書寫系統)) {
          const idx = ['漢語拼音', '國際音標'].indexOf(選項.書寫系統);
          if (is('宕攝 陽韻')) return ['io', 'jɔ'][idx];
        }
      }
      if (is('宕攝 陽韻')) return ['üe', 'ɥɛ'][idx];
      if (is('宕攝 唐韻 開口 牙喉音')) return ['e', 'ɤ'][idx];
      if (is('宕攝 唐韻 一等 幫組')) return ['o', 'u̯o'][idx];
      if (is('宕攝 唐韻')) return ['uo', 'wo'][idx];
      if (is('梗攝 庚韻 二等 幫組')) return ['o', 'u̯o'][idx];
      if (is('梗攝 耕韻 二等 幫組')) return ['o', 'u̯o'][idx];
      if (is('梗攝 二等 開口')) return ['e', 'ɤ'][idx];
      if (is('梗攝 二等')) return ['uo', 'wo'][idx];
      if (is('梗攝 合口')) return ['ü', 'y'][idx];
      if (is('梗攝')) return ['i', 'i'][idx];
      if (is('曾攝 登韻 開口')) return ['e', 'ɤ'][idx];
      if (is('曾攝 登韻 幫組')) return ['o', 'u̯o'][idx];
      if (is('曾攝 登韻')) return ['uo', 'wo'][idx];
      if (is('曾攝 蒸韻 合口')) return ['ü', 'y'][idx];
      if (is('曾攝 蒸韻 莊組')) return ['e', 'ɤ'][idx];
      if (is('曾攝 蒸韻')) return ['i', 'i'][idx];
      if (is('深攝 侵韻 莊組')) return ['e', 'ɤ'][idx];
      if (is('深攝 侵韻')) return ['i', 'i'][idx];
      if (is('咸攝 覃韻 牙喉音')) return ['e', 'ɤ'][idx];
      if (is('咸攝 覃韻')) return ['a', 'a'][idx];
      if (is('咸攝 談韻 牙喉音')) return ['e', 'ɤ'][idx];
      if (is('咸攝 談韻')) return ['a', 'a'][idx];
      if (is('咸攝 鹽韻 知組') && !is('孃母')) return ['e', 'ɤ'][idx];
      if (is('咸攝 鹽韻 莊組')) return ['e', 'ɤ'][idx];
      if (is('咸攝 鹽韻 章組')) return ['e', 'ɤ'][idx];
      if (is('咸攝 鹽韻 日母')) return ['e', 'ɤ'][idx];
      if (is('咸攝 鹽韻')) return ['ie', 'jɛ'][idx];
      if (is('咸攝 添韻')) return ['ie', 'jɛ'][idx];
      if (is('咸攝 咸韻 牙喉音')) return ['ia', 'ja'][idx];
      if (is('咸攝 咸韻')) return ['a', 'a'][idx];
      if (is('咸攝 銜韻 牙喉音')) return ['ia', 'ja'][idx];
      if (is('咸攝 銜韻')) return ['a', 'a'][idx];
      if (is('咸攝 嚴韻')) return ['ie', 'jɛ'][idx];
      if (is('咸攝 凡韻 牙喉音')) return ['ia', 'ja'][idx];
      if (is('咸攝 凡韻')) return ['a', 'a'][idx];
    }
  }
  if (選項.入聲韻分化層次 === '白讀層') {
    if (['漢語拼音', '國際音標'].includes(選項.書寫系統)) {
      const idx = ['漢語拼音', '國際音標'].indexOf(選項.書寫系統);
      if (is('通攝 東韻 一等')) return ['ou', 'oʊ'][idx]; // 讀
      if (is('通攝 東韻 三等 知組')) return ['ou', 'oʊ'][idx];
      if (is('通攝 東韻 三等 莊組')) return ['ou', 'oʊ'][idx];
      if (is('通攝 東韻 三等 章組')) return ['ou', 'oʊ'][idx];
      if (is('通攝 東韻 三等 日母')) return ['ou', 'oʊ'][idx];
      if (is('通攝 東韻 三等')) return ['iou', 'joʊ'][idx]; // 六宿
      if (is('通攝 鍾韻 三等')) return ['ü', 'y'][idx]; // 綠
      if (is('通攝 三等')) return ['iou', 'joʊ'][idx];
      if (is('通攝 一等')) return ['u', 'u'][idx];
      if (is('江攝 江韻 二等 幫組')) return ['ao', 'ɑʊ'][idx]; // 剝
      if (is('江攝 江韻 二等')) return ['iao', 'jɑʊ'][idx]; // 角
      if (is('江攝 江韻 牙喉音')) return ['iao', 'jɑʊ'][idx];
      if (is('江攝 江韻')) return ['ao', 'ɑʊ'][idx];
      if (is('臻攝 眞韻 合口 三等')) return ['uai', 'waɪ'][idx]; // 蟀
      if (is('臻攝 眞韻 合口 莊組')) return ['uai', 'waɪ'][idx];
      if (is('臻攝 眞韻 合口')) return ['ü', 'y'][idx];
      if (is('臻攝 眞韻')) return ['i', 'i'][idx];
      if (is('臻攝 臻韻')) return ['e', 'ɤ'][idx];
      if (is('臻攝 文韻')) return ['ü', 'y'][idx];
      if (is('臻攝 欣韻')) return ['i', 'i'][idx];
      if (is('臻攝 元韻 開口')) return ['ie', 'jɛ'][idx];
      if (is('臻攝 元韻 牙喉音')) return ['üe', 'ɥɛ'][idx];
      if (is('臻攝 元韻')) return ['a', 'a'][idx];
      if (is('臻攝 魂韻 一等')) return ['ei', 'eɪ'][idx];
      if (is('臻攝 魂韻 幫組')) return ['o', 'u̯o'][idx];
      if (is('臻攝 魂韻')) return ['u', 'u'][idx];
      if (is('臻攝 痕韻')) return ['e', 'ɤ'][idx];
      if (is('山攝 寒韻 開口 一等')) return ['a', 'a'][idx]; // 達
      if (is('山攝 寒韻 合口 一等')) return ['uei', 'weɪ'][idx]; // 撮
      // if (is('山攝 寒韻 合口 一等')) return ['uo', 'wo'][idx];
      if (is('山攝 寒韻 一等')) return ['a', 'a'][idx];
      if (is('山攝 寒韻 開口 牙喉音')) return ['e', 'ɤ'][idx];
      if (is('山攝 寒韻 開口')) return ['a', 'a'][idx];
      if (is('山攝 寒韻')) return ['uo', 'wo'][idx];
      if (is('山攝 刪韻 合口')) return ['ua', 'wa'][idx];
      if (is('山攝 刪韻 牙喉音')) return ['ia', 'ja'][idx];
      if (is('山攝 刪韻')) return ['a', 'a'][idx];
      if (is('山攝 山韻 合口')) return ['ua', 'wa'][idx];
      if (is('山攝 山韻 牙喉音')) return ['ia', 'ja'][idx];
      if (is('山攝 山韻')) return ['a', 'a'][idx];
      if (is('山攝 先韻 合口 四等')) return ['ie', 'jɛ'][idx]; // 血
      if (is('山攝 先韻 合口')) return ['üe', 'ɥɛ'][idx];
      if (is('山攝 先韻')) return ['ie', 'jɛ'][idx];
      if (is('山攝 仙韻 合口')) return ['üe', 'ɥɛ'][idx];
      if (is('山攝 仙韻')) return ['ie', 'jɛ'][idx];
      if (is('宕攝 陽韻 開口 三等 幫組')) return ['ao', 'ɑʊ'][idx];
      if (is('宕攝 陽韻 開口 三等 知組')) return ['ao', 'ɑʊ'][idx];
      if (is('宕攝 陽韻 開口 三等 莊組')) return ['ao', 'ɑʊ'][idx];
      if (is('宕攝 陽韻 開口 三等 章組')) return ['ao', 'ɑʊ'][idx];
      if (is('宕攝 陽韻 開口 三等 日母')) return ['ao', 'ɑʊ'][idx];
      if (is('宕攝 陽韻 開口 三等')) return ['iao', 'jɑʊ'][idx];
      if (is('宕攝 陽韻 幫組')) return ['ao', 'ɑʊ'][idx];
      if (is('宕攝 陽韻')) return ['iao', 'jɑʊ'][idx];
      if (is('宕攝 唐韻 開口 一等 見母')) return ['ai', 'aɪ'][idx]; // 閣
      if (is('宕攝 唐韻 開口 一等')) return ['ao', 'ɑʊ'][idx]; // 落
      if (is('宕攝 唐韻 一等')) return ['o', 'o'][idx];
      if (is('宕攝 唐韻 開口 牙喉音')) return ['e', 'ɤ'][idx];
      if (is('宕攝 唐韻')) return ['uo', 'wo'][idx];
      if (is('梗攝 庚韻 開口 二等')) return ['ai', 'aɪ'][idx];
      if (is('梗攝 庚韻 二等')) return ['ai', 'aɪ'][idx];
      if (is('梗攝 耕韻 開口 二等')) return ['ie', 'jɛ'][idx]; // 隔
      if (is('梗攝 耕韻 合口 二等')) return ['uai', 'waɪ'][idx]; // 摑獲
      if (is('梗攝 清韻 開口 三等')) return ['ie', 'jɛ'][idx]; // 掖
      if (is('梗攝 合口 二等')) return ['uai', 'waɪ'][idx];
      if (is('梗攝 二等')) return ['ai', 'aɪ'][idx];
      if (is('梗攝 合口')) return ['ü', 'y'][idx];
      if (is('梗攝')) return ['i', 'i'][idx];
      if (is('曾攝 蒸韻 開口 三等')) return ['ai', 'aɪ'][idx]; // 色
      if (is('曾攝 蒸韻 合口')) return ['ü', 'y'][idx];
      if (is('曾攝 蒸韻 莊組')) return ['e', 'ɤ'][idx];
      if (is('曾攝 蒸韻')) return ['i', 'i'][idx];
      if (is('曾攝 登韻 開口 一等')) return ['ei', 'eɪ'][idx]; // 得
      if (is('曾攝 登韻 合口 一等')) return ['uei', 'weɪ'][idx];
      if (is('曾攝 登韻 開口')) return ['e', 'ɤ'][idx];
      if (is('曾攝 登韻')) return ['uo', 'wo'][idx];
      if (is('曾攝 開口 一等')) return ['ei', 'eɪ'][idx];
      if (is('曾攝 開口 三等')) return ['ai', 'aɪ'][idx];
      if (is('深攝 侵韻 開口 三等 日母')) return ['-i', 'ɻ̍'][idx]; // 入（日 = 肏）
      if (is('深攝 侵韻 開口 三等')) return ['ei', 'eɪ'][idx]; // 給
      if (is('深攝 侵韻 莊組')) return ['e', 'ɤ'][idx];
      if (is('深攝 侵韻')) return ['i', 'i'][idx];
      if (is('深攝 開口 三等')) return ['ei', 'eɪ'][idx];
      if (is('咸攝 覃韻 牙喉音')) return ['e', 'ɤ'][idx];
      if (is('咸攝 覃韻')) return ['a', 'a'][idx];
      if (is('咸攝 談韻 牙喉音')) return ['e', 'ɤ'][idx];
      if (is('咸攝 談韻')) return ['a', 'a'][idx];
      if (is('咸攝 鹽韻')) return ['ie', 'jɛ'][idx];
      if (is('咸攝 添韻 開口 四等')) return ['ei', 'eɪ'][idx]; // 帖
      if (is('咸攝 添韻')) return ['ie', 'jɛ'][idx];
      if (is('咸攝 咸韻 牙喉音')) return ['ia', 'ja'][idx];
      if (is('咸攝 咸韻')) return ['a', 'a'][idx];
      if (is('咸攝 銜韻 牙喉音')) return ['ia', 'ja'][idx];
      if (is('咸攝 銜韻')) return ['a', 'a'][idx];
      if (is('咸攝 嚴韻')) return ['ie', 'jɛ'][idx];
      if (is('咸攝 凡韻 牙喉音')) return ['ia', 'ja'][idx];
      if (is('咸攝 凡韻')) return ['a', 'a'][idx];
    }
  }
  if (選項.入聲韻分化層次 === '文讀層') {
    if (['漢語拼音', '國際音標'].includes(選項.書寫系統)) {
      const idx = ['漢語拼音', '國際音標'].indexOf(選項.書寫系統);
      if (is('通攝 一等')) return ['u', 'u'][idx];
      if (is('通攝 三等 精組')) return ['u', 'u'][idx];
      if (is('通攝 三等 知組')) return ['u', 'u'][idx];
      if (is('通攝 三等 莊組')) return ['u', 'u'][idx];
      if (is('通攝 三等 章組')) return ['u', 'u'][idx];
      if (is('通攝 三等 日母')) return ['u', 'u'][idx];
      if (is('通攝 三等')) return ['ü', 'y'][idx];
      if (is('江攝 江韻 牙喉音')) return ['üe', 'ɥɛ'][idx];
      if (is('江攝 江韻 幫組')) return ['o', 'u̯o'][idx];
      if (is('江攝 江韻')) return ['uo', 'wo'][idx];
      if (is('臻攝 眞韻 合口 莊組')) return ['uai', 'waɪ'][idx];
      if (is('臻攝 眞韻 合口')) return ['ü', 'y'][idx];
      if (is('臻攝 眞欣韻')) return ['i', 'i'][idx];
      if (is('臻攝 臻痕韻')) return ['e', 'ɤ'][idx];
      if (is('臻攝 魂韻 幫組')) return ['o', 'o'][idx];
      if (is('臻攝 魂韻')) return ['u', 'u'][idx];
      if (is('臻攝 文韻')) return ['ü', 'y'][idx];
      if (is('臻攝 元韻 開口')) return ['ie', 'jɛ'][idx];
      if (is('臻攝 元韻 牙喉音')) return ['üe', 'ɥɛ'][idx];
      if (is('臻攝 元韻')) return ['a', 'a'][idx];
      if (is('山攝 寒韻 開口 牙喉音')) return ['e', 'ɤ'][idx];
      if (is('山攝 寒韻 開口')) return ['a', 'a'][idx];
      if (is('山攝 寒韻 一等 幫組')) return ['o', 'u̯o'][idx];
      if (is('山攝 寒韻')) return ['uo', 'wo'][idx];
      if (is('山攝 刪山韻 合口')) return ['ua', 'wa'][idx];
      if (is('山攝 刪山韻 牙喉音')) return ['ia', 'ja'][idx];
      if (is('山攝 刪山韻')) return ['a', 'a'][idx];
      if (is('山攝 仙先韻 合口')) return ['üe', 'ɥɛ'][idx];
      if (is('山攝 仙先韻')) return ['ie', 'jɛ'][idx];
      if (is('宕攝 陽韻 幫組')) return ['o', 'o'][idx];
      if (is('宕攝 陽韻 知組')) return ['uo', 'wo'][idx];
      if (is('宕攝 陽韻 莊組')) return ['uo', 'wo'][idx];
      if (is('宕攝 陽韻 章組')) return ['uo', 'wo'][idx];
      if (is('宕攝 陽韻 日母')) return ['uo', 'wo'][idx];
      if (is('宕攝 陽韻')) return ['üe', 'ɥɛ'][idx];
      if (is('宕攝 唐韻 開口 牙喉音')) return ['e', 'ɤ'][idx];
      if (is('宕攝 唐韻 一等 幫組')) return ['o', 'u̯o'][idx];
      if (is('宕攝 唐韻')) return ['uo', 'wo'][idx];
      if (is('梗攝 庚韻 二等 幫組')) return ['o', 'u̯o'][idx];
      if (is('梗攝 耕韻 二等 幫組')) return ['o', 'u̯o'][idx];
      if (is('梗攝 二等 開口')) return ['e', 'ɤ'][idx];
      if (is('梗攝 二等')) return ['uo', 'wo'][idx];
      if (is('梗攝 合口')) return ['ü', 'y'][idx];
      if (is('梗攝')) return ['i', 'i'][idx];
      if (is('曾攝 登韻 開口')) return ['e', 'ɤ'][idx];
      if (is('曾攝 登韻')) return ['uo', 'wo'][idx];
      if (is('曾攝 蒸韻 合口')) return ['ü', 'y'][idx];
      if (is('曾攝 蒸韻 莊組')) return ['e', 'ɤ'][idx];
      if (is('曾攝 蒸韻')) return ['i', 'i'][idx];
      if (is('深攝 侵韻 莊組')) return ['e', 'ɤ'][idx];
      if (is('深攝 侵韻')) return ['i', 'i'][idx];
      if (is('咸攝 覃談韻 牙喉音')) return ['e', 'ɤ'][idx];
      if (is('咸攝 覃談韻')) return ['a', 'a'][idx];
      if (is('咸攝 鹽添嚴韻')) return ['ie', 'jɛ'][idx];
      if (is('咸攝 咸銜凡韻 牙喉音')) return ['ia', 'ja'][idx];
      if (is('咸攝 咸銜凡韻')) return ['a', 'a'][idx];
    }
  }
  if (選項.入聲韻分化層次 === '暴力白讀層') {
    if (['漢語拼音', '國際音標'].includes(選項.書寫系統)) {
      const idx = ['漢語拼音', '國際音標'].indexOf(選項.書寫系統);
      if (is('通攝 東韻 一等')) return ['ou', 'oʊ'][idx]; // 讀
      if (is('通攝 東韻 三等 知組')) return ['ou', 'oʊ'][idx];
      if (is('通攝 東韻 三等 莊組')) return ['ou', 'oʊ'][idx];
      if (is('通攝 東韻 三等 章組')) return ['ou', 'oʊ'][idx];
      if (is('通攝 東韻 三等 日母')) return ['ou', 'oʊ'][idx];
      if (is('通攝 東韻 三等')) return ['iou', 'joʊ'][idx]; // 六宿
      if (is('通攝 鍾韻 三等')) return ['ü', 'y'][idx]; // 綠
      if (is('通攝 三等')) return ['iou', 'joʊ'][idx];
      if (is('通攝 一等')) return ['u', 'u'][idx];
      if (is('江攝 江韻 二等 幫組')) return ['ao', 'ɑʊ'][idx]; // 剝
      if (is('江攝 江韻 二等')) return ['iao', 'jɑʊ'][idx]; // 角
      if (is('江攝 江韻 牙喉音')) return ['iao', 'jɑʊ'][idx];
      if (is('江攝 江韻')) return ['ao', 'ɑʊ'][idx];
      if (is('臻攝 眞韻 合口 三等')) return ['uai', 'waɪ'][idx]; // 蟀
      if (is('臻攝 眞韻 合口 莊組')) return ['uai', 'waɪ'][idx];
      if (is('臻攝 眞韻 合口')) return ['ü', 'y'][idx];
      if (is('臻攝 眞韻')) return ['i', 'i'][idx];
      if (is('臻攝 臻韻')) return ['e', 'ɤ'][idx];
      if (is('臻攝 文韻')) return ['ü', 'y'][idx];
      if (is('臻攝 欣韻')) return ['i', 'i'][idx];
      if (is('臻攝 元韻 開口')) return ['ie', 'jɛ'][idx];
      if (is('臻攝 元韻 牙喉音')) return ['üe', 'ɥɛ'][idx];
      if (is('臻攝 元韻')) return ['a', 'a'][idx];
      if (is('臻攝 魂韻 一等')) return ['ei', 'eɪ'][idx];
      if (is('臻攝 魂韻 幫組')) return ['o', 'u̯o'][idx];
      if (is('臻攝 魂韻')) return ['u', 'u'][idx];
      if (is('臻攝 痕韻')) return ['e', 'ɤ'][idx];
      if (is('山攝 寒韻 開口 一等')) return ['a', 'a'][idx]; // 達
      if (is('山攝 寒韻 合口 一等')) return ['uei', 'weɪ'][idx]; // 撮
      // if (is('山攝 寒韻 合口 一等')) return ['uo', 'wo'][idx];
      if (is('山攝 寒韻 一等')) return ['a', 'a'][idx];
      if (is('山攝 寒韻 開口 牙喉音')) return ['e', 'ɤ'][idx];
      if (is('山攝 寒韻 開口')) return ['a', 'a'][idx];
      if (is('山攝 寒韻')) return ['uo', 'wo'][idx];
      if (is('山攝 刪韻 合口')) return ['ua', 'wa'][idx];
      if (is('山攝 刪韻 牙喉音')) return ['ia', 'ja'][idx];
      if (is('山攝 刪韻')) return ['a', 'a'][idx];
      if (is('山攝 山韻 合口')) return ['ua', 'wa'][idx];
      if (is('山攝 山韻 牙喉音')) return ['ia', 'ja'][idx];
      if (is('山攝 山韻')) return ['a', 'a'][idx];
      if (is('山攝 先韻 合口 四等')) return ['ie', 'jɛ'][idx]; // 血
      if (is('山攝 先韻 合口')) return ['üe', 'ɥɛ'][idx];
      if (is('山攝 先韻')) return ['ie', 'jɛ'][idx];
      if (is('山攝 仙韻 合口')) return ['üe', 'ɥɛ'][idx];
      if (is('山攝 仙韻')) return ['ie', 'jɛ'][idx];
      if (is('宕攝 陽韻 開口 三等 幫組')) return ['ao', 'ɑʊ'][idx];
      if (is('宕攝 陽韻 開口 三等 知組')) return ['ao', 'ɑʊ'][idx];
      if (is('宕攝 陽韻 開口 三等 莊組')) return ['ao', 'ɑʊ'][idx];
      if (is('宕攝 陽韻 開口 三等 章組')) return ['ao', 'ɑʊ'][idx];
      if (is('宕攝 陽韻 開口 三等 日母')) return ['ao', 'ɑʊ'][idx];
      if (is('宕攝 陽韻 開口 三等')) return ['iao', 'jɑʊ'][idx];
      if (is('宕攝 陽韻 幫組')) return ['ao', 'ɑʊ'][idx];
      if (is('宕攝 陽韻')) return ['iao', 'jɑʊ'][idx];
      if (is('宕攝 唐韻 開口 一等')) return ['a', 'a'][idx]; // 落
      if (is('宕攝 唐韻 一等')) return ['o', 'o'][idx];
      if (is('宕攝 唐韻 開口 牙喉音')) return ['e', 'ɤ'][idx];
      if (is('宕攝 唐韻')) return ['uo', 'wo'][idx];
      if (is('梗攝 庚韻 開口 二等')) return ['ai', 'aɪ'][idx];
      if (is('梗攝 庚韻 二等')) return ['ai', 'aɪ'][idx];
      if (is('梗攝 耕韻 開口 二等')) return ['ie', 'jɛ'][idx]; // 隔
      if (is('梗攝 耕韻 合口 二等')) return ['uai', 'waɪ'][idx]; // 摑獲
      if (is('梗攝 清韻 開口 三等')) return ['ie', 'jɛ'][idx]; // 掖
      if (is('梗攝 合口 二等')) return ['uai', 'waɪ'][idx];
      if (is('梗攝 二等')) return ['ai', 'aɪ'][idx];
      if (is('梗攝 合口')) return ['ü', 'y'][idx];
      if (is('梗攝')) return ['i', 'i'][idx];
      if (is('曾攝 蒸韻 開口 三等')) return ['ai', 'aɪ'][idx]; // 色
      if (is('曾攝 蒸韻 合口')) return ['ü', 'y'][idx];
      if (is('曾攝 蒸韻 莊組')) return ['e', 'ɤ'][idx];
      if (is('曾攝 蒸韻')) return ['i', 'i'][idx];
      if (is('曾攝 登韻 開口 一等')) return ['ei', 'eɪ'][idx]; // 得
      if (is('曾攝 登韻 合口 一等')) return ['uei', 'weɪ'][idx];
      if (is('曾攝 登韻 開口')) return ['e', 'ɤ'][idx];
      if (is('曾攝 登韻')) return ['uo', 'wo'][idx];
      if (is('曾攝 開口 一等')) return ['ei', 'eɪ'][idx];
      if (is('曾攝 開口 三等')) return ['ai', 'aɪ'][idx];
      if (is('深攝 侵韻 開口 三等 日母')) return ['-i', 'ɻ̍'][idx]; // 入（日 = 肏）
      if (is('深攝 侵韻 開口 三等')) return ['ei', 'eɪ'][idx]; // 給
      if (is('深攝 侵韻 莊組')) return ['e', 'ɤ'][idx];
      if (is('深攝 侵韻')) return ['i', 'i'][idx];
      if (is('深攝 開口 三等')) return ['ei', 'eɪ'][idx];
      if (is('咸攝 覃韻 牙喉音')) return ['e', 'ɤ'][idx];
      if (is('咸攝 覃韻')) return ['a', 'a'][idx];
      if (is('咸攝 談韻 牙喉音')) return ['e', 'ɤ'][idx];
      if (is('咸攝 談韻')) return ['a', 'a'][idx];
      if (is('咸攝 鹽韻')) return ['ie', 'jɛ'][idx];
      if (is('咸攝 添韻 開口 四等')) return ['ei', 'eɪ'][idx]; // 帖
      if (is('咸攝 添韻')) return ['ie', 'jɛ'][idx];
      if (is('咸攝 咸韻 牙喉音')) return ['ia', 'ja'][idx];
      if (is('咸攝 咸韻')) return ['a', 'a'][idx];
      if (is('咸攝 銜韻 牙喉音')) return ['ia', 'ja'][idx];
      if (is('咸攝 銜韻')) return ['a', 'a'][idx];
      if (is('咸攝 嚴韻')) return ['ie', 'jɛ'][idx];
      if (is('咸攝 凡韻 牙喉音')) return ['ia', 'ja'][idx];
      if (is('咸攝 凡韻')) return ['a', 'a'][idx];
    }
  }
}

function 舒聲聲調規則() {
  if (['聲調符號', '四角標圈', '五度標記折綫式', '五度標記數字式', '方案序數', '不標調'].includes(選項.標調方式)) {
    const idx = ['聲調符號', '四角標圈', '五度標記折綫式', '五度標記數字式', '方案序數', '不標調'].indexOf(選項.標調方式);
    if (is('清音 平聲')) return ['1', '꜀', '˥', '⁵⁵', '¹', ''][idx];
    if (is('濁音 平聲')) return ['2', '꜁', '˧˥', '³⁵', '²', ''][idx];
    if (is('清音 上聲')) return ['3', '꜂', '˨˩˦', '²¹⁴', '³', ''][idx];
    if (!選項.全濁上歸去) {
      if (['聲調符號', '四角標圈', '五度標記折綫式', '五度標記數字式', '方案序數', '不標調'].includes(選項.標調方式)) {
        const idx = ['聲調符號', '四角標圈', '五度標記折綫式', '五度標記數字式', '方案序數', '不標調'].indexOf(選項.標調方式);
        if (is('全濁 上聲')) return ['3', '꜂', '˨˩˦', '²¹⁴', '³', ''][idx];
      }
    }
    if (is('全濁 上聲')) return ['4', '꜄', '˥˩', '⁵¹', '⁴', ''][idx];
    if (is('次濁 上聲')) return ['3', '꜂', '˨˩˦', '²¹⁴', '³', ''][idx];
    if (is('去聲')) return ['4', '꜄', '˥˩', '⁵¹', '⁴', ''][idx];
  }
}

function 入聲聲調規則() {
  if (選項.清聲母入聲調分派層次 === 'A 皆派入上聲') {
    if (['聲調符號', '四角標圈', '五度標記折綫式', '五度標記數字式', '方案序數', '不標調'].includes(選項.標調方式)) {
      const idx = ['聲調符號', '四角標圈', '五度標記折綫式', '五度標記數字式', '方案序數', '不標調'].indexOf(選項.標調方式);
      if (is('清音 入聲')) return ['3', '꜀', '˥', '²¹⁴', '³', ''][idx];
      if (is('全濁 入聲')) return ['2', '꜁', '˧˥', '³⁵', '²', ''][idx];
      if (is('次濁 入聲')) return ['4', '꜄', '˥˩', '⁵¹', '⁴', ''][idx];
    }
  }
  if (選項.清聲母入聲調分派層次 === 'B 皆派入陰平') {
    if (['聲調符號', '四角標圈', '五度標記折綫式', '五度標記數字式', '方案序數', '不標調'].includes(選項.標調方式)) {
      const idx = ['聲調符號', '四角標圈', '五度標記折綫式', '五度標記數字式', '方案序數', '不標調'].indexOf(選項.標調方式);
      if (is('清音 入聲')) return ['1', '꜀', '˥', '⁵⁵', '¹', ''][idx];
      if (is('全濁 入聲')) return ['2', '꜁', '˧˥', '³⁵', '²', ''][idx];
      if (is('次濁 入聲')) return ['4', '꜄', '˥˩', '⁵¹', '⁴', ''][idx];
    }
  }
  if (選項.清聲母入聲調分派層次 === 'C 除影母和擦音聲母字派入去聲，其餘派入陽平') {
    if (['聲調符號', '四角標圈', '五度標記折綫式', '五度標記數字式', '方案序數', '不標調'].includes(選項.標調方式)) {
      const idx = ['聲調符號', '四角標圈', '五度標記折綫式', '五度標記數字式', '方案序數', '不標調'].indexOf(選項.標調方式);
      if (is('心母 入聲')) return ['4', '꜄', '˥˩', '⁵¹', '⁴', ''][idx];
      if (is('生母 入聲')) return ['4', '꜄', '˥˩', '⁵¹', '⁴', ''][idx];
      if (is('書母 入聲')) return ['4', '꜄', '˥˩', '⁵¹', '⁴', ''][idx];
      if (is('曉母 入聲')) return ['4', '꜄', '˥˩', '⁵¹', '⁴', ''][idx];
      if (is('影母 入聲')) return ['4', '꜄', '˥˩', '⁵¹', '⁴', ''][idx];
      if (is('全清 入聲')) return ['2', '꜁', '˧˥', '³⁵', '²', ''][idx];
      if (is('次清 入聲')) return ['4', '꜄', '˥˩', '⁵¹', '⁴', ''][idx];
      if (is('全濁 入聲')) return ['2', '꜁', '˧˥', '³⁵', '²', ''][idx];
      if (is('次濁 入聲')) return ['4', '꜄', '˥˩', '⁵¹', '⁴', ''][idx];
    }
  }
  if (選項.清聲母入聲調分派層次 === 'D 除影母字派入去聲，其餘派入陽平') {
    if (['聲調符號', '四角標圈', '五度標記折綫式', '五度標記數字式', '方案序數', '不標調'].includes(選項.標調方式)) {
      const idx = ['聲調符號', '四角標圈', '五度標記折綫式', '五度標記數字式', '方案序數', '不標調'].indexOf(選項.標調方式);
      if (is('影母 入聲')) return ['4', '꜄', '˥˩', '⁵¹', '⁴', ''][idx];
      if (is('全清 入聲')) return ['2', '꜁', '˧˥', '³⁵', '²', ''][idx];
      if (is('次清 入聲')) return ['4', '꜄', '˥˩', '⁵¹', '⁴', ''][idx];
      if (is('全濁 入聲')) return ['2', '꜁', '˧˥', '³⁵', '²', ''][idx];
      if (is('次濁 入聲')) return ['4', '꜄', '˥˩', '⁵¹', '⁴', ''][idx];
    }
  }
  if (選項.清聲母入聲調分派層次 === 'E 連同濁聲母、所有入聲調全部派入去聲') {
    if (['聲調符號', '聲調符號（注音）', '四角標圈', '五度標記折綫式', '五度標記數字式', '方案序數', '不標調'].includes(選項.標調方式)) {
      const idx = ['聲調符號', '聲調符號（注音）', '四角標圈', '五度標記折綫式', '五度標記數字式', '方案序數', '不標調'].indexOf(選項.標調方式);
      if (is('入聲')) return ['4', '꜄', '˥˩', '⁵¹', '⁴', ''][idx];
    }
  }
}

let 聲母 = 聲母推導規則();
let 韻母 = is('舒聲') ? 舒聲韻母規則() : 入聲韻母規則();
let 聲調 = is('舒聲') ? 舒聲聲調規則() : 入聲聲調規則();

if (選項.書寫系統 === '漢語拼音') {
  if (!選項.分尖團) {
    if (['i', 'ü'].includes(韻母[0])) 聲母 = {
      g: 'j', k: 'q', h: 'x',
      z: 'j', c: 'q', s: 'x',
    }[聲母] || 聲母;
  }
  if (選項.漢語拼音注釋改寫) {
    // 《漢語拼音方案》注釋1：“知、蚩、詩、日、資、雌、思”等七個音節的韻母用 i，即：知、蚩、詩、日、資、雌、思等字拼作 zhi, chi, shi, ri, zi, ci, si。
    if (韻母 === '-i') {韻母 = 'i';}
    // 《漢語拼音方案》注釋4：i 列的韻母，前面沒有聲母的時候，寫成 yi（衣）、ya（呀）、ye（耶）、yao（腰）、you（優）、yan（煙）、yin（因）、yang（央）、ying（英）、yong（雍）。
    if (!聲母) {
      if (韻母 === 'i') {聲母 = 'y'; 韻母 = 'i';}
      if (韻母 === 'in') {聲母 = 'y'; 韻母 = 'in';}
      if (韻母 === 'ing') {聲母 = 'y'; 韻母 = 'ing';}
    }
    if (!聲母) {if (韻母.startsWith('i')) {聲母 = 'y'; 韻母 = 韻母.slice(1);}}
    // 《漢語拼音方案》注釋10：ㄨㄥ有兩種寫法：ㄨㄥ若之前沒有加其他聲母時，單純的發爲“ㄨ+ㄥ”（/wəŋ/），此時拼音寫成ueng。若前方有聲母，則會發爲“ㄨ+ㄫ”（/ʊŋ/），此時拼音寫成-ong（而非-ueng）。
    if (!聲母) {if (韻母 === 'ong') {聲母 = ''; 韻母 = 'ueng';}}
    // 《漢語拼音方案》注釋5：u 列的韻母，前面沒有聲母的時候，寫成 wu（烏）、wa（蛙）、wo（窩）、wai（歪）、wei（威）、wan（彎）、wen（溫）、wang（汪）、weng（翁）。
    if (!聲母) {if (韻母 === 'u') {聲母 = 'w'; 韻母 = 'u';}}
    if (!聲母) {if (韻母.startsWith('u')) {聲母 = 'w'; 韻母 = 韻母.slice(1);}}
    // 《漢語拼音方案》注釋6：ü 列的韻母，前面沒有聲母的時候，寫成 yu（迂）、yue（約）、yuan（冤）、yun（暈）；ü 上兩點省略。
    if (!聲母) {if (韻母.startsWith('ü')) {聲母 = 'y'; 韻母 = 'u' + 韻母.slice(1);}}
    // 《漢語拼音方案》注釋7：ü 列的韻母跟聲母 j、q、x 拼的時候，寫成 ju（居）、qu（區）、xu（虛），ü上兩點省略；但是跟聲母 l、n 拼的時候，仍然寫成 lü（呂）、nü（女）。
    if (韻母.startsWith('ü')) {
      if (聲母 === 'j') {韻母 = 'u' + 韻母.slice(1);}
      if (聲母 === 'q') {韻母 = 'u' + 韻母.slice(1);}
      if (聲母 === 'x') {韻母 = 'u' + 韻母.slice(1);}
    }
    // 《漢語拼音方案》注釋8：iou、uei、uen 前面加聲母的時候，寫成 iu、ui、un，例如 niu（牛）、gui（歸）、lun（論）。
    if (聲母) {
      if (韻母 === 'iou') {韻母 = 'iu';}
      if (韻母 === 'uei') {韻母 = 'ui';}
      if (韻母 === 'uen') {韻母 = 'un';}
    }
  }
  // 止攝日母字合法需要
  if (聲母 === 'r') {if (韻母 === 'er') {聲母 = ''; 韻母 = 'er';}}
  if (聲母 === 'r') {if (韻母 === 'io') {聲母 = ''; 韻母 = 'io';}}
  // 《漢語拼音方案》注釋9：在給漢字注音的時候，爲了使拼式簡短，ng 可以省作 ŋ，zh、sh、ch 可省作 ẑ、ŝ、ĉ。
  if (選項.應用注釋9) {
    if (聲母 === 'ng') 聲母 = 'ŋ';
    if (韻母.endsWith('ng')) 韻母 = 韻母.slice(0, -2) + 'ŋ';
    if (聲母 === 'zh') 聲母 = 'ẑ';
    if (聲母 === 'ch') 聲母 = 'ĉ';
    if (聲母 === 'sh') 聲母 = 'ŝ';
  }
  if (選項.標調方式 === '聲調符號') return 聲母 + (聲調 ? 韻母.replace(/(.*)[aüê]|(.*)[eo]|(.*)[iu]/, "$&" + " ̄́̌̀"[聲調]) : 韻母);
  if (選項.標調方式 === '四角標圈') return [...'꜀꜁꜂꜃'].includes(聲調) ? 聲調 + 聲母 + 韻母 : 聲母 + 韻母 + 聲調;
  if (選項.標調方式 === '五度標記折綫式') return 聲母 + 韻母 + 聲調;
  if (選項.標調方式 === '五度標記數字式') return 聲母 + 韻母 + 聲調;
  if (選項.標調方式 === '方案序數') return 聲母 + 韻母 + 聲調;
  if (選項.標調方式 === '不標調') return 聲母 + 韻母;
}

if (選項.書寫系統 === '國際音標') {
  if (!選項.分尖團) {
    if (['i', 'j', 'y', 'ɥ'].includes(韻母[0])) 聲母 = {
      k: 'tɕ', kʰ: 'tɕʰ', x: 'ɕ',
      ts: 'tɕ', tsʰ: 'tɕʰ', s: 'ɕ',
    }[聲母] || 聲母;
  }
  // 止攝日母字合法需要
  if (聲母 === 'ɻ') {if (韻母 === 'əɻ') {聲母 = ''; 韻母 = 'əɻ';}}
  if (聲母 === 'ɻ') {if (韻母 === 'jɔ') {聲母 = ''; 韻母 = 'jɔ';}}
  if (選項.標調方式 === '聲調符號') {
    if (聲調 === '1') 聲調 = 'ˉ';
    if (聲調 === '2') 聲調 = 'ˊ';
    if (聲調 === '3') 聲調 = 'ˇ';
    if (聲調 === '4') 聲調 = 'ˋ';
    return 聲母 + 韻母 + 聲調;
  }
  if (選項.標調方式 === '四角標圈') return [...'꜀꜁꜂꜃'].includes(聲調) ? 聲調 + 聲母 + 韻母 : 聲母 + 韻母 + 聲調;
  if (選項.標調方式 === '五度標記折綫式') return 聲母 + 韻母 + 聲調;
  if (選項.標調方式 === '五度標記數字式') return 聲母 + 韻母 + 聲調;
  if (選項.標調方式 === '方案序數') return 聲母 + 韻母 + 聲調;
  if (選項.標調方式 === '不標調') return 聲母 + 韻母;
}

/* 检查文本
 *
 * 遙襟甫暢，逸興遄飛。爽籟發而清風生，纖歌凝而白雲遏。睢園綠竹，氣凌彭澤之樽；鄴水朱華，光照臨川之筆。四美具，二難并。窮睇眄於中天，極娛遊於暇日。天高地迥，覺宇宙之無窮；興盡悲來，識盈虛之有數。望長安於日下，目吳會於雲間。地勢極而南溟深，天柱高而北辰遠。關山難越，誰悲失路之人。萍水相逢，盡是他鄉之客。懷帝閽而不見，奉宣室以何年？
 * 1、知蚩詩日資雌思
 * 4、衣呀耶腰優煙因央英雍
 * 10：翁
 * 5：烏蛙窩歪威彎溫汪翁
 * 6：迂約冤暈
 * 7：居區虛呂女
 * 8：牛歸論
 * 
 * 他大
 * 熟宿軸勺讀白泊沒藥落脈鑿抹躍翟捽瀑約悖惡咋泄藉黑塞平上
 * 勒忸綠六玉殼摑閣給入拆柏獲肉撮隔掖液色牛虐方
 * 帖血
 * 剝雀蟀角腳窄百學迫
 * 
 * 鹤朱蛛不诸
 * 可
 * 
 * 虐略着逴杓弱爵鵲削膠卻學壳藥岳
 * 皆揩鞋街解介楷鞵骇邂埃涯矮隘
 */
